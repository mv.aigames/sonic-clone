// Fill out your copyright notice in the Description page of Project Settings.


#include "Controllers/CharacterController.h"

#include "Kismet/GameplayStatics.h"
#include "Camera/PlayerCameraManager.h"

#include "Characters/BaseCharacter.h"
#include "SonicClone/Interfaces/ActionsSet.h"

void ACharacterController::OnPossess(APawn* newPawn)
{
    Super::OnPossess(newPawn);

    if (ABaseCharacter* ch = Cast<ABaseCharacter>(newPawn))
    {
        character = ch;
        actionsSet = ch->GetActionsSet();
        if (actionsSet)
        {
            BindActionsAndAxis(character);
        }
        else
        {
            UE_LOG(LogTemp, Error, TEXT("CharacterController needs a IActionsSet interface"));
        }
    }
}

void ACharacterController::BindActionsAndAxis(ABaseCharacter* ch)
{
    // InputComponent->BindVectorAxis("Move", this, &ACharacterController::OnMove);
    InputComponent->BindAxis("MoveRight", this, &ACharacterController::OnMoveRight);
    InputComponent->BindAxis("MoveUp", this, &ACharacterController::OnMoveUp);
    InputComponent->BindAction("Jump", EInputEvent::IE_Pressed, this, &ACharacterController::OnBottomAction);
}

void ACharacterController::OnMove(FVector axis)
{
    UE_LOG(LogTemp, Warning, TEXT("Axis: %s"), *axis.ToString());
}

void ACharacterController::OnMoveRight(float value)
{
    float realValue = FixGamePadInput(value);
    // UE_LOG(LogTemp, Warning, TEXT("Axis Right: %f"), realValue);
    direction.X = realValue;
}

void ACharacterController::OnMoveUp(float value)
{
    float realValue = FixGamePadInput(value);
    // UE_LOG(LogTemp, Warning, TEXT("Axis Up: %f"), realValue);
    direction.Y = realValue;
}


void ACharacterController::PostProcessInput(float DeltaTime, const bool bGamePaused)
{
    // UE_LOG(LogTemp, Warning, TEXT("POst Input"));
    return;
    FRotator cameraRotator = UGameplayStatics::GetPlayerCameraManager(GetWorld(), 0)->GetCameraRotation();
    FVector facingCamera = FVector(FMath::Cos(cameraRotator.Yaw), FMath::Sin(cameraRotator.Yaw), 0.0f);
    UE_LOG(LogTemp, Warning, TEXT("FAcing CAmera: %s"), *facingCamera.ToString());
    FVector rightVector = FVector::CrossProduct(facingCamera, GetPawn()->GetActorUpVector());
    FVector finalDirection = (facingCamera * -direction.Y) + (rightVector * -direction.X);
    // character->OnMoveDirectionSet.Broadcast(finalDirection);
}


float ACharacterController::FixGamePadInput(float rawValue)
{
    float realValue = (rawValue - 0.5f) / 0.5f;
    realValue = FMath::Abs(realValue) > deathZone ? realValue : 0.0f;
    return realValue;
}

void ACharacterController::OnBottomAction()
{
    actionsSet->BottomAction();
}