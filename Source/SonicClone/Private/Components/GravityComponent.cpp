// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/GravityComponent.h"

#include "Characters/BaseCharacter.h"

// Sets default values for this component's properties
UGravityComponent::UGravityComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UGravityComponent::BeginPlay()
{
	Super::BeginPlay();

	if (ABaseCharacter* bc = Cast<ABaseCharacter>(GetOwner()))
	{
		
	}
}


// Called every frame
void UGravityComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	FVector newVelocity = acceleration * direction * DeltaTime;

	Cast<ABaseCharacter>(GetOwner())->OnMotionAdd.Broadcast(newVelocity);
}

void UGravityComponent::SetDirection(FVector newDirection)
{
	direction = newDirection;
}


void UGravityComponent::SetAcceleration(float newAcceleration)
{
	acceleration = newAcceleration;
}