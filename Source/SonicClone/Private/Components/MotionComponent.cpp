// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/MotionComponent.h"

#include "Characters/BaseCharacter.h"

#include "Kismet/GameplayStatics.h"

// Sets default values for this component's properties
UMotionComponent::UMotionComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	bWantsInitializeComponent = true;

	
}

void UMotionComponent::InitializeComponent()
{
	Super::InitializeComponent();

	character = Cast<ABaseCharacter>(GetOwner());
	if (character)
	{
		UE_LOG(LogTemp, Warning, TEXT("Added Character???????"));
		// character->OnMoveDirectionSet.AddDynamic(this, &UMotionComponent::OnDirectionSet);
		character->OnMotionAdd.AddUObject(this, &UMotionComponent::OnMotionAdded);
		character->OnMotionSet.AddUObject(this, &UMotionComponent::OnMotionSet);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Motino must be attached to ABaseCharacter"));
		SetComponentTickEnabled(false);
	}
}


// Called when the game starts
// void UMotionComponent::BeginPlay()
// {
// 	Super::BeginPlay();
// }


// Called every frame
void UMotionComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	FHitResult hit;
	character->AddActorWorldOffset(motion * DeltaTime, true, &hit);

	if (hit.GetActor())
	{
		character->OnCollisionEnter.Broadcast(hit.GetActor(), hit.GetComponent(), character);
		if (FVector::DotProduct(hit.Normal, character->GetActorUpVector()) > 0.9f)
		{
			character->OnGroundHit.Broadcast();
			
			// TODO: Improved ground hit motion cancel.
			// motion += character->GetActorUpVector() * motion.Z; 
			motion.Z = 0.0f;
		}
	}
}

void UMotionComponent::OnMotionSet(const FVector& newMotion)
{
	motion = newMotion;
}


void UMotionComponent::OnMotionAdded(const FVector& addedMotion)
{
	// UE_LOG(LogTemp, Warning, TEXT("Motion Added: %s"), *motion.ToString());
	motion += addedMotion;
}