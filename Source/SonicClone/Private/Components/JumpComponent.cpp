// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/JumpComponent.h"

#include "Characters/BaseCharacter.h"

// Sets default values for this component's properties
UJumpComponent::UJumpComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	bWantsInitializeComponent = true;
	// ...
}

void UJumpComponent::InitializeComponent()
{
	Super::InitializeComponent();

	if (ABaseCharacter* ch = Cast<ABaseCharacter>(GetOwner()))
	{
		character = ch;
		character->OnGroundHit.AddUObject(this, &UJumpComponent::GroundHit);
	}

}

// Called when the game starts
void UJumpComponent::BeginPlay()
{
	Super::BeginPlay();

	
}

void UJumpComponent::Jump()
{
	UE_LOG(LogTemp, Warning, TEXT("On Jump"));
	character->OnMotionSet.Broadcast(character->GetActorUpVector() * jumpForce);
}


void UJumpComponent::GroundHit()
{
	jumps = 0;
}
