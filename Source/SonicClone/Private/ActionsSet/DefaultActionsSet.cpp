// Fill out your copyright notice in the Description page of Project Settings.


#include "ActionsSet/DefaultActionsSet.h"

#include "Characters/BaseCharacter.h"
#include "Components/JumpComponent.h"

// Sets default values for this component's properties
UDefaultActionsSet::UDefaultActionsSet()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	bWantsInitializeComponent = true;
	// ...
}

void UDefaultActionsSet::InitializeComponent()
{
	Super::InitializeComponent();
	character = Cast<ABaseCharacter>(GetOwner());
	if (character)
	{
		jumpComponent = Cast<UJumpComponent>(character->FindComponentByClass<UJumpComponent>());
		if (!jumpComponent)
		{
			UE_LOG(LogTemp, Error, TEXT("DefaultActionsSet needs a JumpComponent in order to work properly"));
		}
	}
}


// Called when the game starts
void UDefaultActionsSet::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UDefaultActionsSet::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UDefaultActionsSet::TopAction()
{

}

void UDefaultActionsSet::BottomAction()
{
	jumpComponent->Jump();
}

void UDefaultActionsSet::LeftAction()
{
	
}

void UDefaultActionsSet::RightAction()
{
	
}

void UDefaultActionsSet::MoveAction()
{
	
}
