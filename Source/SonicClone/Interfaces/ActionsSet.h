// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "ActionsSet.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UActionsSet : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class SONICCLONE_API IActionsSet
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION()
	virtual void BottomAction() = 0;
	UFUNCTION()
	virtual void TopAction() = 0;
	UFUNCTION()
	virtual void LeftAction() = 0;
	UFUNCTION()
	virtual void RightAction() = 0;
	UFUNCTION()
	virtual void MoveAction() = 0;
};
