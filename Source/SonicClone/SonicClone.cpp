// Copyright Epic Games, Inc. All Rights Reserved.

#include "SonicClone.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SonicClone, "SonicClone" );
