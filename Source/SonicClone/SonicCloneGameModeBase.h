// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SonicCloneGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SONICCLONE_API ASonicCloneGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
