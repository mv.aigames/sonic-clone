// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SonicClone/Interfaces/ActionsSet.h"
#include "DefaultActionsSet.generated.h"


UCLASS( ClassGroup=(ActionsSet), meta=(BlueprintSpawnableComponent), Blueprintable, BlueprintType )
class SONICCLONE_API UDefaultActionsSet : public UActorComponent, public IActionsSet
{
	GENERATED_BODY()


protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	
public:	
	UDefaultActionsSet();

	virtual void InitializeComponent() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// IActionsSet
	virtual void BottomAction() override;
	virtual void TopAction() override;
	virtual void LeftAction() override;
	virtual void RightAction() override;
	virtual void MoveAction() override;

	class ABaseCharacter* character;
	class UJumpComponent* jumpComponent;
};
