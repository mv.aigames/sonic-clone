// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "CharacterController.generated.h"

/**
 * 
 */
UCLASS()
class SONICCLONE_API ACharacterController : public APlayerController
{
	GENERATED_BODY()

	public:
		virtual void PostProcessInput(float DeltaTime, const bool bGamePaused) override;

	protected:
		virtual void OnPossess(APawn* newPawn) override;	
	
	private:
		void BindActionsAndAxis(class ABaseCharacter* character);
		UFUNCTION()
		void OnMove(FVector axis);

		UFUNCTION()
		void OnMoveRight(float value);

		UFUNCTION()
		void OnMoveUp(float value);

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GamePad", meta = (AllowPrivateAccess = "true"))
		float deathZone = 0.2f;

		float FixGamePadInput(float rawValue);

		class ABaseCharacter* character;

		FVector direction;

		class UCameraComponent* playerCamera;
		class IActionsSet* actionsSet;
		void OnBottomAction();
};
