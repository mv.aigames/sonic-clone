// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Components/ActorComponent.h"
#include "Components/PrimitiveComponent.h"
#include "BaseCharacter.generated.h"

DECLARE_MULTICAST_DELEGATE_ThreeParams(FCollisionEnter, AActor*, UPrimitiveComponent*, AActor*);
DECLARE_MULTICAST_DELEGATE_ThreeParams(FCollisionExit, AActor*, UPrimitiveComponent*, AActor*);
DECLARE_MULTICAST_DELEGATE(FGroundHit);
DECLARE_MULTICAST_DELEGATE_OneParam(FMotionSet, const FVector&);
DECLARE_MULTICAST_DELEGATE_OneParam(FMotionAdd, const FVector&);

UCLASS()
class SONICCLONE_API ABaseCharacter : public APawn
{
	GENERATED_BODY()


	public:	
		ABaseCharacter();
		
		virtual void Tick(float DeltaTime) override;

		FCollisionEnter OnCollisionEnter;
		FCollisionExit OnCollisionExit;
		FMotionSet OnMotionSet;
		FMotionAdd OnMotionAdd;
		FGroundHit OnGroundHit;

		class IActionsSet* GetActionsSet();

	protected:
		// Called when the game starts or when spawned
		virtual void BeginPlay() override;

	private:
		UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Body", meta = (AllowPrivateAccess = "true"))
		class UCapsuleComponent* capsuleComponent;

		UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Body", meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* meshComponent;

		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "State", meta = (AllowPrivateAccess = "true"))
		FVector velocity;
};
