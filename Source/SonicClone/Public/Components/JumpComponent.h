// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "JumpComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SONICCLONE_API UJumpComponent : public UActorComponent
{
	GENERATED_BODY()

	public:	
		// Sets default values for this component's properties
		UJumpComponent();
		void Jump();
		virtual void InitializeComponent() override;

	protected:
		// Called when the game starts
		virtual void BeginPlay() override;

	private:

		UFUNCTION()
		void GroundHit();

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Jump", meta = (AllowPrivateAccess = "true"))
		int32 maxJump = 1;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Jump", meta = (AllowPrivateAccess = "true"))
		float jumpForce = 100.0f;

		UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Jump", meta = (AllowPrivateAccess = "true"))
		int32 jumps;

		class ABaseCharacter* character;
};
