// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GravityComponent.generated.h"


UCLASS( ClassGroup=(BaseCharacter), meta=(BlueprintSpawnableComponent) )
class SONICCLONE_API UGravityComponent : public UActorComponent
{
	GENERATED_BODY()


	protected:
		// Called when the game starts
		virtual void BeginPlay() override;
	
	public:	
		
		UGravityComponent();
		
		virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		void SetDirection(FVector newDirection);
		void SetAcceleration(float newAcceleration);

	private:
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gravity", meta = (AllowPrivateAccess = "true"))
		float acceleration = 98.0f;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gravity", meta = (AllowPrivateAccess = "true"))
		FVector direction = FVector::DownVector;

};
