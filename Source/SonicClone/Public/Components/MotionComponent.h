// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MotionComponent.generated.h"



UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SONICCLONE_API UMotionComponent : public UActorComponent
{
	GENERATED_BODY()

	public:	
		// Sets default values for this component's properties
		UMotionComponent();	
		// Called every frame
		virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
		virtual void InitializeComponent() override;


	protected:
		// Called when the game starts
		// virtual void BeginPlay() override;


	private:
		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Motion", meta = (AllowPrivateAccess = "true"))
		FVector motion;

		UFUNCTION()
		void OnMotionAdded(const FVector& addedMotion);

		UFUNCTION()
		void OnMotionSet(const FVector& newMotion);

		class ABaseCharacter* character;
};
